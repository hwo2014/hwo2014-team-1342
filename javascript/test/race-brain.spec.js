"use strict";

var expect = require("expect.js");
var RaceBrain = require("../lib/sor-race-logic/race-brain");

describe('Brain', function() {
    var raceBrain;
    var defaultCurveAngle = 45.0;
    before(function(){
        raceBrain = new RaceBrain();
        raceBrain._curveBrakeDistance = 100.0;
        raceBrain._curveEnterVelocity = 5.0;
        raceBrain._topSpeed = 10.0;
    });

    describe('_calculateBrakeDistance', function(){

        it('should return default brake distance if car is driving top speed', function(){
            var result = raceBrain._calculateBrakeDistance(raceBrain._topSpeed, raceBrain._curveEnterVelocity);
            expect(result).to.be(raceBrain._curveBrakeDistance);
        });

        it('should return half brake distance if velocity is exactly between top speed and curve enter speed', function() {
            var velocity = (raceBrain._topSpeed - raceBrain._curveEnterVelocity) / 2.0 + raceBrain._curveEnterVelocity;
            var result = raceBrain._calculateBrakeDistance(velocity, raceBrain._curveEnterVelocity);
            expect(result).to.be(raceBrain._curveBrakeDistance / 2.0);
        });

        it('should return quarter brake distance if velocity is exactly between top speed and curve enter speed and curve enter velocity is between default curve enter velocity and velocity', function() {
            var velocity = (raceBrain._topSpeed - raceBrain._curveEnterVelocity) / 2.0 + raceBrain._curveEnterVelocity;
            var result = raceBrain._calculateBrakeDistance(velocity, (raceBrain._curveEnterVelocity - velocity) / 2 + velocity);
            expect(result).to.be(raceBrain._curveBrakeDistance / 4.0);
        });

        it('should return zero brake distance if car is driving slower than curve enter velocity', function(){
            var velocity = 2.0;
            var result = raceBrain._calculateBrakeDistance(velocity, raceBrain._curveEnterVelocity);
            expect(result).to.be(0);
        });

    });

    describe('calculateCurveEnterVelocity', function() {
        it('should return 1.25 * velocity if angle of the curve is half of the default angle', function() {
            var result = raceBrain._calculateCurveEnterVelocity(defaultCurveAngle / 2.0);
            expect(result).to.be(raceBrain._curveEnterVelocity * 1.25);
        });

        it('should return 1.25 * velocity if angle of the curve is half of the default angle but negative', function() {
            var result = raceBrain._calculateCurveEnterVelocity(-defaultCurveAngle / 2.0);
            expect(result).to.be(raceBrain._curveEnterVelocity * 1.25);
        });

    });

});