var expect = require("expect.js");
var SuperOffRoad = require("../lib/super-off-road");

describe('SuperOffRoad', function() {
    var superOffRoad;
    before(function(){
        superOffRoad = new SuperOffRoad();
        superOffRoad.myCar = {
            "name": "Schumacher",
            "color": "red"
        };

    });

    describe('_getOwnCarIndex', function(){
        it('should return index of own car data', function(){

            var data = [
                {
                    "id": {
                        "name": "Rosberg",
                        "color": "blue"
                    }
                },
                {
                    "id": {
                        "name": "Schumacher",
                        "color": "red"
                    }
                }
            ];
            expect(superOffRoad._getOwnCarPositionIndex(data)).to.be(1);
        });
    });

    describe('_getPositions', function(){
        it('should return location data of own and opponents cars', function(){

            var data = [
                {
                    "id": {
                        "name": "Rosberg",
                        "color": "blue"
                    }
                },
                {
                    "id": {
                        "name": "Schumacher",
                        "color": "red"
                    }
                },
                {
                    "id": {
                        "name": "Raikkonen",
                        "color": "white"
                    }
                }
            ];

            var result = superOffRoad._getPositions(data);
            var ownCar = result.ownCar;
            var opponents = result.opponents;
            expect(ownCar.id.name).to.be(superOffRoad.myCar.name);
            expect(opponents.length).to.be(2);
        });
    });

});