"use strict";

var expect = require("expect.js");
var TrackKnowledge = require("../lib/track-knowledge");

describe('TrackKnowledge', function() {
    var trackKnowledge;
    before(function(){
        trackKnowledge = new TrackKnowledge();
    });

    describe('_arcLength', function(){
        it('should return length of arc', function(){
            var arcLength = trackKnowledge._arcLength(45.0, 100.0);
            expect(arcLength.toPrecision(5)).to.be('78.540');
        });

        it('should return length of arc with negative angle', function(){
            var arcLength = trackKnowledge._arcLength(-45.0, 100.0);
            expect(arcLength.toPrecision(5)).to.be('78.540');
        });

    });
});