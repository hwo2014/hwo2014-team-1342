"use strict";

var net = require("net");
var JSONStream = require('JSONStream');
var _ = require('lodash');
var util = require('util');

var SuperOffRoad = require("./lib/super-off-road");
var log = require('./lib/race-log');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

var command = process.argv[6]; //join or create. Not required.
var trackName = process.argv[7];
var password = process.argv[8];
var carCount = process.argv[9];
var logicType = process.argv[10];
var outputType = process.argv[11];

if(!_.isUndefined(outputType) && outputType !== null && outputType.length > 0) {
    log.logOutputType = outputType;
}

if(trackName === "null") {trackName = null;}
if(password === "null") { password = null;}

if(command === "join" && password !== null) {
    botName += "-" +makeId(5);
}

log.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

var superOffRoad = new SuperOffRoad();
superOffRoad.logicType = logicType;



var client = net.connect(serverPort, serverHost, function() {
    log.log("Connected. Joining to a game...");

    var startPacket;
    if(command  === "create") {

        if(_.isUndefined(trackName)) {
            log.log("Set trackName");
            return;
        }
        if(_.isUndefined(password)) {
            log.log("Set password");
            return;
        }

        if(_.isUndefined(carCount)) {
            log.log("Set carCount");
            return;
        }

        log.log("Creating a new race...");
        log.log("Track: " + trackName);
        log.log("Password to join: " + password);
        log.log("Car count: " + carCount);

        startPacket = {
            msgType:  "createRace",
            data: {
                botId: {
                    name: botName,
                    key: botKey
                },
                trackName: trackName,
                password: password,
                carCount: carCount
            }};
    } else if(command  === "join") {

        if(_.isUndefined(trackName)) {
            log.log("Set trackName");
            return;
        }

        if(_.isUndefined(carCount)) {
            log.log("Set carCount");
            return;
        }

        log.log("Joining to a race...")
        log.log("Track: " + trackName);
        log.log("Password: " + password);
        log.log("Car count: " + carCount);

        startPacket = {
            msgType:  "joinRace",
            data: {
                botId: {
                    name: botName,
                    key: botKey
                },
                carCount: parseInt(carCount)
            }};

        if(!_.isUndefined(password) && password !== null) {startPacket.data.password = password;}
        if(!_.isUndefined(trackName) && trackName !== null) {startPacket.data.trackName = trackName;}
    } else {
        // Default join
        startPacket = {
            msgType: "join",
            data: {
                name: botName,
                key: botKey
            }};
    }

    return send(startPacket);
});


var jsonStream = client.pipe(JSONStream.parse());

var receivedMessageMapping = {
    "join":         { handler: superOffRoad.onJoin(),         callback: sendPing}, // sent after joining using simple join protocol
    "joinRace":     { handler: superOffRoad.onJoin(),         callback: sendPing}, // sent after joining using advanced join protocol
    "yourCar":      { handler: superOffRoad.onYourCar(),      callback: sendPing },
    "gameInit":     { handler: superOffRoad.onGameInit(),     callback: sendPing },
    "gameStart":    { handler: superOffRoad.onGameStart(),    callback: sendPing },
    "carPositions": { handler: superOffRoad.onCarPositions(), callback: sendRaceAction },
    "gameEnd":      { handler: superOffRoad.onGameEnd(),      callback: sendPing },
    "tournamentEnd": {handler: superOffRoad.onTournamentEnd(),callback: sendPing },
    "crash":        { handler: superOffRoad.onCrash(),        callback: sendPing },
    "spawn":         { handler: superOffRoad.onSpawn(),       callback: sendPing },
    "lapFinished":  { handler: superOffRoad.onLapFinished(),  callback: sendPing },
    "dnf":          { handler: superOffRoad.onDnf(),          callback: sendPing },
    "finish":       { handler: superOffRoad.onFinish(),       callback: sendPing },
    "turboAvailable": { handler: superOffRoad.onTurboAvailable(), callback: sendPing },
    "turboStart": { handler: superOffRoad.onTurboStart(), callback: function(){} },
    "error":        { handler: serverError,                   callback: function(){}}
};

jsonStream.on('data', function(data) {

    if(!_.has(data, 'msgType')) {
        log.log("Unable to act. No msgType on received message.");
        return;
    }
    var msgType = data.msgType;

    var msgData = null;
    if(_.has(data, 'data')) {
        msgData = data.data;
        // TODO: Check that game id is for our game
        // TODO: Check that tick is about something that it should be
    }

    if(_.has(receivedMessageMapping, msgType)) {
        var mapping = receivedMessageMapping[msgType];
        var gameTickOrCallback = mapping.callback;
        if(_.has(data, 'gameTick')) {
            gameTickOrCallback = data.gameTick;
        }
        mapping.handler(msgData, gameTickOrCallback, function(err, result) {

            if(!_.isUndefined(result) && _.isNumber(gameTickOrCallback)) {

                if(outputType === 'csv') {
                    if(gameTickOrCallback === 1) {
                        log.log('Tick; Throttle; Velocity; SlipAngle; CurveDistance; CurveAngle; CurveSlopeness; CurveLaneRadius', 'csv');
                    }
                    log.log(gameTickOrCallback.toFixed(0) +
                        ";" + result.throttle.toFixed(2).replace(".", ",") +
                        ";" + result.velocity.toFixed(2).replace(".", ",") +
                        ";" + result.slipAngle.toFixed(2).replace(".", ",") +
                        ";" + result.curveDistance.toFixed(2).replace(".", ",") +
                        ";" + result.curveAngle.toFixed(2).replace(".", ",") +
                        ";" + result.curveSlopeness.toFixed(2).replace(".", ",") +
                        ";" + result.curveLaneRadius.toFixed(2).replace(".", ","),
                    'csv');
                } else {
                    log.log("@" + gameTickOrCallback.toFixed(0) +
                            "|#" + result.pieceIndex +
                            "|SL" + result.startLaneIndex +
                            "|EL" + result.endLaneIndex +
                            "|T " + result.throttle.toFixed(2) +
                            "|V " + result.velocity.toFixed(2) +
                            "|CD " + result.curveDistance.toFixed(2) +
                            "|SLI " + result.slipAngleSigned.toFixed(2) +
                            "|SLI5 " + result.slipForecastSigned[4].toFixed(2) +
                            "|SLI10 " + result.slipForecastSigned[9].toFixed(2) +
                            "|DS " + result.deltaSlipAngle.toFixed(3) +
                            "|D2S " + result.delta2SlipAngle.toFixed(3) +
                            "|D3S " + result.delta3SlipAngle.toFixed(3) +
                            "|OPP " + result.nearestSameLaneOpponentAheadDistance.toFixed(1) +
                            "|DIFF " + result.nearestSameLaneOpponentAheadVelocityDiff.toFixed(2)

                    );

                }
            }

            mapping.callback(err, result, gameTickOrCallback);
        });
    } else {
        log.log("Unknown msgType: " + msgType);
    }

});

jsonStream.on('error', function() {
    return log.log("Error. Disconnected.");
});

jsonStream.on('end', function() {
    return log.log("End stream.");
});

function serverError(data, gameTickOrCallback, callback) {
    if(_.isFunction(gameTickOrCallback)) {
        callback = gameTickOrCallback;
    }
    log.log("Server error");
    log.log(util.inspect(data));
    return callback(null);
}

function sendPing(err) {
    if(err) {
        log.log("Error on AI (sendPing): " + err);
    }

    send({
        msgType: "ping",
        data: {}
    });
}

function sendRaceAction (err, action, gameTick) {
    if(err) {
        log.log("Error on AI (sendRaceAction): " + err);
    }

    // The server processes only one message per tick so we need to prioritize

    // 1. Turbo
    if(action.useTurbo) {
        log.log("TURBO!");
        return send({
            msgType: "turbo",
            data: "Turbo!",
            gameTick: gameTick
        });
    }

    // 2. Lane switch
    if(action.switchLane !== null && action.switchLane.length > 0) {
        log.log("Send switch " + action.switchLane);
        return send({
            msgType: "switchLane",
            data: action.switchLane,
            gameTick: gameTick
        });
    }

    // 3. Throttle amount
    send({
        msgType: "throttle",
        data: action.throttle,
        gameTick: gameTick
    });
}

function send(json) {
    client.write(JSON.stringify(json));
    return client.write('\n');
}

function makeId(length)
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < length; i++ ) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}