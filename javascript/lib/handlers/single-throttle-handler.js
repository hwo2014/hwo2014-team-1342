"use strict";

var throttle = 0.95;


module.exports = function(params, action) {
    action.throttle = throttle;
    action.useTurbo = false;
};