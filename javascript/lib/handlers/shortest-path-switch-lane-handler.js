"use strict";

var util = require('util');
var _ = require('lodash');


module.exports = function(params, action) {
    var telemetry = params.telemetry;
    var pieceLanePath = params.pieceLanePath;

    // Switch logic
    if(telemetry.pieceChanged) {
        // Inform lane switching one piece before the switch piece
        if(!_.isUndefined(pieceLanePath.shortest) && pieceLanePath.shortest.shortestSwitch !== 0) {
            action.switchLane = pieceLanePath.shortest.shortestSwitch;
        }
    }
};