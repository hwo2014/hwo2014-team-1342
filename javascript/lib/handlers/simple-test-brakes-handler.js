"use strict";

var util = require('util');
var _ = require('lodash');
var log = require("../race-log");

var config = {
    minTestBrakesVelocity: 6.5,
    ticksToTestBrakes: 3,
    minCurveDistanceToTestBrakes: 10
};

module.exports = function(params, action) {
    var self = this;
    var telemetry = params.telemetry;

    // Test brakes if not tested yet
    if( telemetry.calculatedDecelerationCount < config.ticksToTestBrakes &&
        telemetry.velocity > config.minTestBrakesVelocity &&
        telemetry.curveDistance > config.minCurveDistanceToTestBrakes) {

        log.log("Testing brakes");
        action.throttle = 0.0;
    }
};
