"use strict";
var log = require("../race-log");

/**
 * Minimum straight for turbo. TODO: scale this using car length.
 * @type {number}
 * @private
 */
var turboMinStraightPerDurationPerFactor = 4;
var turboMaxSlip = 40;
var turboMaxDeltaSlip = -0.1;


module.exports = function(params, action) {
    // Turbo logic
    var self = this;
    var telemetry = params.telemetry;
    var turbo = params.turbo;

    if(turbo !== null) {
        var turboMinCurveDistance = (turboMinStraightPerDurationPerFactor * turbo.turboDurationTicks * turbo.turboFactor);
        log.log("Turbo min curve distance: " + turboMinCurveDistance);

        if(telemetry.curveDistance >= turboMinCurveDistance) {
            if(!telemetry.isCurve && telemetry.slipAngle <= turboMaxSlip && telemetry.deltaSlipAngle <= turboMaxSlip) {
                log.log("Set turbo ON");
                action.useTurbo = true;
            }
        }
    }
};