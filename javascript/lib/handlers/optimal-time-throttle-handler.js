"use strict";

var util = require('util');
var _ = require('lodash');
var log = require("../race-log");

var config = {
    brakeOffThrottle: 0.5,
    curveEnterVelocity: 7.0,    // Trying to enter curve this velocity
    minThrottle: 0.0,           // Throttle is always at least this amount
    minSlipAngle: 0.2,
    maxSlipForecastAngle: 40.0,         // No throttle at any point if slipping more than this
    maxAbsDeltaSlipAngle: 3.5,
    minDeltaSlipAngle: 0.1,
    maxDeltaSlipAngle: 1.0,     // No throttle if abs slip angle delta is bigger than this
    curveMaxVelocity: 10.0      // Maximum velocity when we're on a curve piece
};



module.exports = function(params, action) {
    var telemetry = params.telemetry;
    var nextCurve =  params.nextCurve;

    //TODO: Slip handling on straight

    // Throttle logic
    if(!telemetry.isCurve) {
        // We're on straight
        // Entering to curve logic
        var nextCurveAngle = telemetry.nextCurveAngle;
        var nextCurveTotalAngle = telemetry.nextCurveTotalAngle;
        var curveEnterVelocity = calculateCurveEnterVelocity(nextCurveAngle, nextCurveTotalAngle);

        var estimatedBrakeDistance = calculateBrakeDistance(telemetry, curveEnterVelocity);
        log.log("Angle: " + nextCurveAngle.toFixed(2) +
            ", tot: " + nextCurveTotalAngle +
            ", cEV: " + curveEnterVelocity.toFixed(2) + ", eBD: " +
            estimatedBrakeDistance.toFixed(2)  + ", dec: " + telemetry.avgDecelerationOnStraightPerTick);

        if(telemetry.curveDistance <= estimatedBrakeDistance) {
            if(telemetry.velocity > curveEnterVelocity) {
                log.log("Brake ON");
                action.throttle = config.minThrottle;
            } else {
                log.log("Brake OFF");
                action.throttle = config.brakeOffThrottle;
            }
        } else {
            // Long distance to next curve. Full speed ahead.
            action.throttle = 1.0;
        }
    } else {

        // We're on curve

        // Slip handling logic

        // When to brake
        if(telemetry.deltaSlipAngle > config.maxDeltaSlipAngle ||// Slip is increasing
            telemetry.absDeltaSlipAngle > config.maxAbsDeltaSlipAngle ||              // Takaisinheiaus
            telemetry.slipAngle > config.maxSlipForecastAngle &&       // Already slipping a lot
            ((telemetry.slipAngle > config.minSlipAngle &&          // Some minimum values to consider whether we are slipping
                telemetry.deltaSlipAngle > config.minDeltaSlipAngle) ||
                telemetry.velocity > config.curveMaxVelocity)) {
            action.throttle = config.minThrottle;
        } else {
            action.throttle = 1.0;
        }

    }
};

function calculateCurveEnterVelocity(curveAngle, curveTotalAngle) {
    var curveScaleFactor = 0.8;  // Smaller factor is smaller enter velocity
    var curveVelocityScale = 1 + curveScaleFactor * (1 - (Math.abs(curveAngle) / (45.0)));
    var scaledCurveEnterVelocity = config.curveEnterVelocity * curveVelocityScale;

    if(curveTotalAngle > curveAngle) {
        var totalCurveAnglePer45 = (Math.abs(curveTotalAngle)) / (45.0);
        var factor = 0.25; // bigger factor = slower enter speed
        var scaler = 1- (factor * (totalCurveAnglePer45 - 1));
        scaledCurveEnterVelocity *= scaler;
    }

    return scaledCurveEnterVelocity;
}

function calculateBrakeDistance(telemetry, curveEnterVelocity) {
    var numTicksToBrake = (telemetry.velocity - curveEnterVelocity) / telemetry.avgDecelerationOnStraightPerTick;
    var avgVelocity = ((telemetry.velocity + curveEnterVelocity) / 2.0);
    var result = avgVelocity * numTicksToBrake;
    result = Math.max(0, result);
    //log.log("nttb: " + numTicksToBrake + ", av: " + avgVelocity + ", r: " + result);
    return result;
}
