"use strict";

var util = require('util');
var _ = require('lodash');
var log = require("../race-log");

var config = {
    guardSlipAngle: 10,     // when guard starts to calculate
    maxSlipForecastAngle: 58.5,       // brakes are put on if calculated value is more than this
    ticksToCalculate: 10
};

module.exports = function(params, action) {
    var self = this;
    var telemetry = params.telemetry;

    var ticksToCalculate = config.ticksToCalculate;
    var slip = telemetry.slipAngle;

    if (slip > config.guardSlipAngle) {
        for(var i=0; i<ticksToCalculate; i++) {
            // Loop through the values because latest is not always the largest
            var slipForecast = telemetry.slipForecast[i];
            if(slipForecast >= config.maxSlipForecastAngle) {
                log.log("Slip guard " + i + "!");
                action.throttle = 0.0;
                action.slipGuardActivated = true;
                break;
            }
        }
    }
};