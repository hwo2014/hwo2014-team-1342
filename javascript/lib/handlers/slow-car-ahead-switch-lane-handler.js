"use strict";

var util = require('util');
var _ = require('lodash');

var config = {
    maxTicksToReach: 100,
    minDistance: 70 //TODO: should not be hard coded but readed from car length
};

module.exports = function(params, action) {
    var telemetry = params.telemetry;
    var pieceLanePath = params.pieceLanePath;
    var opponentSpy = params.opponentSpy;

    // Switch logic
    if(telemetry.pieceChanged) {
        if(!_.isUndefined(pieceLanePath.shortest) && (!_.isUndefined(pieceLanePath.shortest.right) || !_.isUndefined(pieceLanePath.shortest.left))) {
            // Switch is possible on next piece
            var opponentDistance = telemetry.nearestSameLaneOpponentAheadDistance;
            var velocityDiff = telemetry.nearestSameLaneOpponentAheadVelocityDiff;
            var ticksToReach = (velocityDiff !== 0) ? opponentDistance / velocityDiff : Number.MAX_VALUE;
            if(velocityDiff < 0 && Math.abs(ticksToReach) < config.maxTicksToReach ||
                opponentDistance < config.minDistance) { //velocityDiff is about 0 if another car is already blocking
                console.log("Switching (maybe) lane to avoid traffic jam");

                // don't switch to shortest track because it's most probable that slow car will switch there too
                if(!_.isUndefined(pieceLanePath.shortest.left) && pieceLanePath.shortest.left !== null && pieceLanePath.shortest.shortestSwitch !== -1) {
                    action.switchLane = -1;
                } else if(pieceLanePath.shortest.shortestSwitch !== 1) {
                    action.switchLane = 1;
                } else {
                    action.switchLane = 0; // removes lane switching if this module is used after shortest path module
                }
            }
        }
    }
};