"use strict";

var util = require('util');
var _ = require('lodash');
var log = require("../race-log");


module.exports = function(params, action) {
    var telemetry = params.telemetry;
    var lastTelemetry = params.lastTelemetry;
    action.useTurbo = false;
    var switchingLane = telemetry.startLaneIndex !== telemetry.endLaneIndex;
    var i;

    var maxVelocity = 100.0;

    var maxDeltaSlip = 3.0;
    if(telemetry.isCurve && !switchingLane && telemetry.deltaSlipAngle < maxDeltaSlip) {
        var maxSlipForecast = 57;
        var slipWarning = false;
        for(i=0; i<10; i++) {
            if (telemetry.slipForecast[i] >= maxSlipForecast) {
                slipWarning = true;
                break;
            }
        }
        if(!slipWarning) {
            log.log("Curve out Acc!");
        } else {
            maxVelocity = telemetry.curveMaxVelocity;
        }
    }

    for(i=0; i<6; i++) { // loop trough next curves
        var curveTelemetry = telemetry.nextCurves[i];
        var curveTargetVelocity = curveTelemetry.curveMaxVelocity;
        var estimatedBrakeDistance = calculateBrakeDistance(telemetry, curveTargetVelocity);
        if(telemetry.velocity > 12.0) { estimatedBrakeDistance += 2*telemetry.velocity;} // on high speed (turbo) we should break little earlier
        if(lastTelemetry.throttle === 0.0 && curveTelemetry.curveDistance < (estimatedBrakeDistance + 2*telemetry.velocity)) {
            // kind of hysteresis to avoid swapping throttle between 0 and 1 on braking
            maxVelocity = Math.min(maxVelocity, curveTargetVelocity);
            // log.log("B " + i);
        } else if(curveTelemetry.curveDistance <= (estimatedBrakeDistance)) {
            maxVelocity = Math.min(maxVelocity, curveTargetVelocity);
            // log.log("A"  + i);
        }
        if(i===0) {
            log.log("Next curve: CD:" + curveTelemetry.curveDistance.toFixed(2) + ", EBD: " + estimatedBrakeDistance.toFixed(2) + " RAD: " + curveTelemetry.curveLaneRadius + " MAXV: " + curveTargetVelocity);
        }
    }

    log.log("Max V: " + maxVelocity + " Cur rad: " + telemetry.curveLaneRadius);
    var velocityDifference = Math.abs(maxVelocity - telemetry.velocity);
    var throttleBalancer = 0.0;
    if(velocityDifference <= 0.1) {
        throttleBalancer = 0.9;
    } else if(velocityDifference <= 0.2) {
        throttleBalancer = 0.7;
    } else if(velocityDifference <= 0.3) {
        throttleBalancer = 0.5;
    }

    if(telemetry.velocity > maxVelocity) {
        action.throttle = 0.0;
    } else {
        action.throttle = 1.0 - throttleBalancer;
    }


};

function calculateBrakeDistance(telemetry, curveEnterVelocity) {

    var calculatedVelocity = telemetry.velocity;
    var numTicksToBrake = 0;
    var distance = 0;
    while(calculatedVelocity > curveEnterVelocity || numTicksToBrake > 999) {
        var oldCalculatedVelocity = calculatedVelocity;
        var deceleration = telemetry.getDeceleration(calculatedVelocity);
        deceleration = Math.max(0.05, deceleration); // on some tracks there comes weird results until enough is learned
        calculatedVelocity -= deceleration;
        distance += (calculatedVelocity + oldCalculatedVelocity) / 2;
        numTicksToBrake++;
    }
    return distance;
}

