"use strict";

var targetVelocity = 4.0;


module.exports = function(params, action) {
    var telemetry = params.telemetry;
    action.useTurbo = false;

    if(parseInt(telemetry.tick) === 110) {
        action.switchLane = 1;
    }

    if(telemetry.velocity < targetVelocity) {
        action.throttle += 0.05;
        action.throttle = Math.min(1.0, action.throttle);
    } else if(telemetry.velocity > targetVelocity) {
        action.throttle -= 0.05;
        action.throttle = Math.max(0.0, action.throttle);
    }

};

