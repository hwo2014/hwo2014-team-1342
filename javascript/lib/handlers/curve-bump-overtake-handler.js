"use strict";
var log = require("../race-log");

/**
 * Does not brake to curve if there's car near ahead
 * @type {number}
 * @private
 */
var maxNearestSameLaneOpponentAheadDistancePerVelocity = 4; // E.g. on speed of 10 the value will be 10 times this.
                                                            // Or this many ticks to crash if opponent would stop now
var maxCurveDistance = 200;
var minCurveDistance = 10;


module.exports = function(params, action) {
    var self = this;
    var telemetry = params.telemetry;

    var maxNearestSameLaneOpponentAheadDistance = maxNearestSameLaneOpponentAheadDistancePerVelocity * telemetry.velocity;
    log.log("Max nearest: " + maxNearestSameLaneOpponentAheadDistance);

    if(telemetry.curveDistance < maxCurveDistance &&
        telemetry.curveDistance > minCurveDistance &&
        telemetry.nearestSameLaneOpponentAheadDistance < maxNearestSameLaneOpponentAheadDistance) {
        log.log("Curve bump");
        action.throttle = 1.0; // No brakes
    }
};