"use strict";

var log = require("../race-log");

var curveMaxDistance = 500;

/**
 * Will use turbo to bump out another car on curve
 * @type {number}
 * @private
 */
module.exports = function(params, action) {
    var self = this;
    var telemetry = params.telemetry;

    if(telemetry.turboAvailable) {
        var maxTurboVelocity = telemetry.velocity * telemetry.turboFactor;
        var velocityWithTurbo = (telemetry.velocity + maxTurboVelocity) / 2; // Estimation
        var ticksToCurve = telemetry.curveDistance / velocityWithTurbo;
        // log.log("Ticks to curve: " + ticksToCurve);

        //TODO: we need opponent velocity here but let's assume it's same than our
        var opponentVelocity = telemetry.velocity;
        var velocityDifference =  velocityWithTurbo - opponentVelocity;

        var maxNearestSameLaneOpponentAheadDistance = ticksToCurve * velocityDifference;
        var curveMinDistance = 0.8*(maxNearestSameLaneOpponentAheadDistance + velocityDifference * ticksToCurve);
        log.log("Min turbo bump curve distance: " + curveMinDistance + ",  max opponent distance: " + maxNearestSameLaneOpponentAheadDistance);

        if(!telemetry.isCurve &&
            telemetry.slipAngle < 8 &&
            telemetry.nearestSameLaneOpponentAheadDistance < maxNearestSameLaneOpponentAheadDistance) {
            if (telemetry.curveDistance < curveMaxDistance &&
                telemetry.curveDistance > curveMinDistance) {
                log.log("Curve turbo bump");
                action.throttle = 1.0; // No brakes
                action.useTurbo = true;
            } else if (telemetry.curveDistance > curveMaxDistance) {
                // Save turbo for curve and bump opponent out
                log.log("Saving turbo for next curve");
                action.useTurbo = false; // Save turbo
            }
        }

    }

};