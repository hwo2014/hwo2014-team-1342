"use strict";

var turboHandler = require('./handlers/simple-turbo-handler');
var switchLaneHandler = require('./handlers/shortest-path-switch-lane-handler');
var singleVelocityHandler = require('./handlers/single-velocity-handler');
var slipGuardHandler = require('./handlers/slip-guard-handler');


module.exports.logic = function(params, resultAction) {
    singleVelocityHandler(params, resultAction);
    switchLaneHandler(params, resultAction);
    slipGuardHandler(params, resultAction);
};

module.exports.name = "Race Logic";
