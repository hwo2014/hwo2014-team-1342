"use strict";

var _ = require('lodash');

/**
 * AI logic for games which are type of race.
 * @param {TrackKnowledge} trackKnowledge
 * @param {OpponentSpy} opponentSpy
 * @param {TelemetryCalculator} telemetryCalculator
 * @param {PathTracker} pathTracker
 * @param {Function} brainLogicFunc (params, resultAction)
 * @constructor
 */
function Brain(trackKnowledge, opponentSpy, telemetryCalculator, pathTracker, brainLogicFunc) {
    if(!_.isFunction(brainLogicFunc)) { throw new Error("brainLogicFunc is not a function");}

    this._opponentSpy = opponentSpy;
    this._brainLogicFunc = brainLogicFunc;
    this._trackKnowledge = trackKnowledge;
    this._telemetryCalculator = telemetryCalculator;
    this._pathTracker = pathTracker;
}

Brain.prototype.getTelemetryCalculator = function() {
    return this._telemetryCalculator;
};

/**
 * Calculates actions to be taken.
 * @param {Object} ownCarPosition    Position data of own car.
 * @param {Array}  opponentPositions Position data of opponents.
 * @param {Number} tick         Game tick
 * @param {Object} params       {turbo: Object}
 * @param {Function} callback   Callback function(err).
 */
Brain.prototype.think = function(ownCarPosition, opponentPositions, tick, params, callback) {
    var self = this;

    // Calculate values
    self._opponentSpy.calculate(ownCarPosition, opponentPositions, tick);
    var lastTelemetry = self._telemetryCalculator.getLatest();
    var telemetry = self._telemetryCalculator.calculate(ownCarPosition, params, tick);
    var pieceLanePath = self._pathTracker.trackPath(ownCarPosition);
    self._trackKnowledge.learnOnRace(ownCarPosition, self._telemetryCalculator.getAll());

    var action = {
        throttle: lastTelemetry.throttle,
        useTurbo: false,
        laneSwitch: null
    };

    var handleParams = {
        lastTelemetry: lastTelemetry,
        telemetry: telemetry,
        pieceLanePath: pieceLanePath,
        ownCarPosition: ownCarPosition,
        opponentPositions: opponentPositions,
        nextCurve: self._trackKnowledge.getNextCurve(ownCarPosition)
    };
    _.assign(handleParams, params);

    self._brainLogicFunc(handleParams, action);

    self._telemetryCalculator.appendTelemetryValues(action);
    return callback(null, telemetry);
};

//-----------------------------------------------------------
// Exports - Class Constructor
//-----------------------------------------------------------
module.exports = Brain;
