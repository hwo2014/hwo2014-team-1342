"use strict";

var _ = require('lodash');


module.exports = {
    logOutputType: "default",

    log: function(text, outputType) {

        if(_.isUndefined(outputType)) { outputType = 'default'; }

        if(outputType === this.logOutputType) {
            console.log(text);
        }
    }
};


