"use strict";

var _ = require('lodash');
var util = require('util');
var log = require('./race-log');

/**
 * @param {TrackKnowledge} trackKnowledge
 * @constructor
 */
function OpponentSpy(trackKnowledge) {
    this._ownData = [];
    this._opponentData = [];
    this._trackKnowledge = trackKnowledge;
    this._pieceVelocities = []; // { id, lap, avgVelocity}
}

OpponentSpy.prototype.getSameLaneOpponentAheadVelocityDiff = function() {
    var self = this;
    var ownData = self._ownData[self._ownData.length-1];
    var opponentData = self._getOpponentOnSameLaneAhead();
    if(opponentData === null) { return 0;}

    // Compare velocities of last two pieces that both have already fully driven
    var maxPieceIndex = Math.min(ownData.pieceIndex, opponentData.pieceIndex) -1;
    if(maxPieceIndex < 0) { maxPieceIndex += self._trackKnowledge.pieceCount(); }
    var minPieceIndex = maxPieceIndex - 1;
    if(minPieceIndex < 0) { minPieceIndex += self._trackKnowledge.pieceCount(); }

    var ownVelocities = _.filter(self._pieceVelocities, function(pieceVelocityInfo) {
        return (pieceVelocityInfo.id.name === ownData.id.name &&
            pieceVelocityInfo.lap === ownData.lap &&
            (pieceVelocityInfo.pieceIndex === minPieceIndex ||
            pieceVelocityInfo.pieceIndex === maxPieceIndex) );
    });
    if(ownVelocities.length === 0) { return 0; } // On crash for example
    var sumOwnVelocities = _.reduce(ownVelocities, function(sum, vel) {return sum + vel.avgVelocity;}, 0);
    var ownAvgVelocity = sumOwnVelocities / ownVelocities.length;
    // console.log("Own avg v: " + ownAvgVelocity);

    var opponentVelocities = _.filter(self._pieceVelocities, function(pieceVelocityInfo) {
        return (pieceVelocityInfo.id.name === opponentData.id.name &&
            pieceVelocityInfo.lap === opponentData.lap &&
            (pieceVelocityInfo.pieceIndex === minPieceIndex ||
                pieceVelocityInfo.pieceIndex === maxPieceIndex) );
    });
    var sumOpponentVelocities = _.reduce(opponentVelocities, function(sum, vel) {return sum + vel.avgVelocity;}, 0);
    var opponentAvgVelocity = sumOpponentVelocities / opponentVelocities.length;
    var velocityDiff = opponentAvgVelocity - ownAvgVelocity;

    // console.log("ov.length: " + opponentVelocities.length);
    // console.log("sumOpponentV: " + sumOpponentVelocities + ", oav: " + opponentAvgVelocity);

    return velocityDiff;
};

/**
 * Returns distance to the opponent on the same lane.
 * @returns {Number} distance to the opponent or Number.MAX_VALUE if there's no results.
 */
OpponentSpy.prototype.getSameLaneOpponentAheadDistance = function() {
    var self = this;
    var ownData = self._ownData[self._ownData.length-1];
    var opponentAheadOnSameLane = self._getOpponentOnSameLaneAhead();
    if(opponentAheadOnSameLane === null) { return Number.MAX_VALUE; };

    var distance = self._trackKnowledge.getDistanceBetween(ownData, opponentAheadOnSameLane);
    return distance;
};

OpponentSpy.prototype.calculate = function(ownCarPosition, opponentCarPositions, tick) {
    var self = this;

    var ownCarData = self._calculateCarPositionData(ownCarPosition, tick);
    var ownLastData = _.find(self._ownData, function(data) {return data.tick === tick-1 && data.id.name === ownCarPosition.id.name;});
    if(!_.isUndefined(ownLastData) && ownLastData !== null && ownLastData.pieceIndex !== ownCarData.pieceIndex) {
        self._calculateCarPieceData(ownLastData);
    }
    self._ownData.push(ownCarData);

    opponentCarPositions.forEach(function(opponentPosition) {
        var carData = self._calculateCarPositionData(opponentPosition, tick);
        var carLastData = _.find(self._opponentData, function(data) {return data.tick === tick-1 && data.id.name === opponentPosition.id.name;});
        if(!_.isUndefined(carLastData) && carLastData !== null && carLastData.pieceIndex !== carData.pieceIndex) {
            self._calculateCarPieceData(carLastData);
        }
        self._opponentData.push(carData);
    });

};

OpponentSpy.prototype._getOpponentOnSameLaneAhead = function() {
    var self = this;

    var ownData = self._ownData[self._ownData.length-1];
    var opponentsOnSameLane = _.filter(self._opponentData, function(opponent) {
        return (opponent.startLaneIndex === ownData.startLaneIndex &&
            opponent.endLaneIndex === ownData.endLaneIndex &&
            opponent.tick === ownData.tick);
    });

    // log.log("opponents on same lane: " + opponentsOnSameLane.length);

    var opponentsAhead = _.filter(opponentsOnSameLane, function(opponent) {
        return (opponent.lap > ownData.lap) || (
            opponent.lap === ownData.lap && (
            opponent.pieceIndex > ownData.pieceIndex ||
            (opponent.pieceIndex === ownData.pieceIndex && opponent.inPieceDistance > ownData.inPieceDistance)
            )
            );
    });

    var opponentsBehind = _.filter(opponentsOnSameLane, function(opponent) {
        return (opponent.lap < ownData.lap) || (
            opponent.lap === ownData.lap && (
            opponent.pieceIndex < ownData.pieceIndex ||
            (opponent.pieceIndex === ownData.pieceIndex && opponent.inPieceDistance < ownData.inPieceDistance)
            )
            );
    });

    // log.log("opponents ahead: " + opponentsAhead.length);
    // log.log("opponents behind: " + opponentsBehind.length);

    var sortedOpponentsAhead = _.sortBy(opponentsAhead, ['lap', 'pieceIndex', 'inPieceDistance']);
    var sortedOpponentsBehind = _.sortBy(opponentsBehind, ['lap', 'pieceIndex', 'inPieceDistance']);
    if(sortedOpponentsAhead.length === 0 && sortedOpponentsBehind.length === 0) {return null;}

    var opponentAhead = sortedOpponentsAhead[0];
    var opponentMostFarBehind = sortedOpponentsBehind[sortedOpponentsBehind.length-1]; // might be one lap behind

    if(sortedOpponentsAhead.length === 0) { return opponentMostFarBehind; }
    if(sortedOpponentsBehind.length === 0) { return opponentAhead; }

    var distanceAhead = self._trackKnowledge.getDistanceBetween(ownData, opponentAhead);
    var distanceBehind = self._trackKnowledge.getDistanceBetween(ownData, opponentMostFarBehind);
    return (distanceAhead <= distanceBehind) ? opponentAhead : opponentMostFarBehind;
};

OpponentSpy.prototype._calculateCarPieceData = function(carLastData) {
    var self = this;

    var filter = function(data) {
        return data.pieceIndex === carLastData.pieceIndex &&
            data.lap === carLastData.lap &&
            data.id.name === carLastData.id.name;
    };

    var pieceDatas = _.filter(self._opponentData, filter);

    if(pieceDatas.length === 0) {
        // Maybe it's our own car
        pieceDatas = _.filter(self._ownData, filter);
    }
    if(pieceDatas.length === 0) {
        console.log("No piece data found for " + carLastData.id.name + " piece: " + carLastData.pieceIndex);
        return;
    }

    var velocitySum = _.reduce(pieceDatas, function(sum, data) { return sum + data.velocity; }, 0);
    var pieceVelocity = velocitySum / pieceDatas.length;
    console.log("Piece " + carLastData.pieceIndex + " velocity: " + pieceVelocity + " " + carLastData.id.name);
    self._pieceVelocities.push({
        id: carLastData.id,
        avgVelocity: + pieceVelocity,
        lap: carLastData.lap,
        pieceIndex: carLastData.pieceIndex
    });
};

OpponentSpy.prototype._calculateCarPositionData = function(carPosition, tick) {
    var data = {
        id: carPosition.id,
        startLaneIndex: carPosition.piecePosition.lane.startLaneIndex,
        endLaneIndex: carPosition.piecePosition.lane.endLaneIndex,
        pieceIndex: carPosition.piecePosition.pieceIndex,
        inPieceDistance: carPosition.piecePosition.inPieceDistance,
        lap: carPosition.piecePosition.lap,
        velocity: carPosition.velocity,
        tick: tick
    };
    return data;
};

module.exports = OpponentSpy;
