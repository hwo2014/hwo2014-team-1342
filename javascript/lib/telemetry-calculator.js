"use strict";

var _ = require('lodash');
var util = require('util');
var log = require('./race-log');
var regression = require('./regression');
var Telemetry = require('./telemetry');

/**
 * Telemetry.
 * @param {TrackKnowledge} trackKnowledge
 * @param {TelemetryCalculator} telemetryCalculator     Qualifying telemetry is used to initialize some race values.
 *                                                      Set to null on qualifying session.
 * @constructor
 */
function TelemetryCalculator(trackKnowledge, opponentSpy, telemetryCalculator) {

    this._opponentSpy = opponentSpy;

    /** Recorded deceleration values. @private {Array} */
    this._deceleration = [];

    this._decelerationEquation = [0.0829, 0.0004]; // Use some default values for first ticks
    if(!_.isUndefined(telemetryCalculator) && telemetryCalculator !== null) {
        this._deceleration.push.apply(this._deceleration, telemetryCalculator._deceleration);
        this._decelerationEquation = telemetryCalculator._decelerationEquation;
        log.log("Learned deceleration values from old telemetry.");
    }

    /** @private {Array} */
    this._telemetries = [];

    /** @private {TrackKnowledge} */
    this._trackKnowledge = trackKnowledge;
}


TelemetryCalculator.prototype.calculate = function(carPosition, params, tick) {
    var self = this;

    var piece = self._trackKnowledge.getPiece(carPosition);

    var turbo = params.turbo;

    var lastTelemetry = self.getLatest();
    var telemetry = new Telemetry();

    telemetry.tick = tick;
    telemetry.pieceIndex = carPosition.piecePosition.pieceIndex;
    telemetry.startLaneIndex = carPosition.piecePosition.lane.startLaneIndex;
    telemetry.endLaneIndex = carPosition.piecePosition.lane.endLaneIndex;
    telemetry.pieceChanged = telemetry.pieceIndex !== lastTelemetry.pieceIndex;


    telemetry.velocity = carPosition.velocity;
    telemetry.deltaVelocity = telemetry.velocity - lastTelemetry.velocity;

    self._calculateCurvesTelemetry(telemetry, carPosition);

    var nextCurve = telemetry.nextCurves[0];


    telemetry.isCurve = piece.isCurve;
    telemetry.curveAngle = (telemetry.isCurve)  ? piece.angle : 0.0;
    telemetry.curveSlopeness = (telemetry.isCurve) ? Math.abs(piece.slopeness) : 0.0;
    telemetry.curveLaneRadius = (telemetry.isCurve) ? Math.abs(piece.laneRadius[telemetry.startLaneIndex]) : 0.0;
    telemetry.curveMaxVelocity = piece.laneMaxVelocities[telemetry.endLaneIndex];

    telemetry.curveDistance = nextCurve.curveDistance;
    telemetry.nextCurveAngle = nextCurve.curveAngle;
    telemetry.nextCurveLaneRadius = nextCurve.curveLaneRadius;
    telemetry.nextCurveSlopeness = nextCurve.curveSlopeness;
    telemetry.nextCurveTotalAngle = nextCurve.curveTotalAngle; // Total angle of next curves without straight between them


    telemetry.slipAngleSigned = _.isUndefined(carPosition.angle) ? 0.0: carPosition.angle;
    telemetry.slipAngle = Math.abs(telemetry.slipAngleSigned);
    telemetry.deltaSlipAngle = telemetry.slipAngle - lastTelemetry.slipAngle;   // Positive means slipping is increasing
    telemetry.deltaSlipAngleSigned = telemetry.slipAngleSigned - lastTelemetry.slipAngleSigned;
    telemetry.delta2SlipAngle = telemetry.deltaSlipAngle - lastTelemetry.deltaSlipAngle; // Positive means slipping is increasing faster than earlier
    telemetry.delta2SlipAngleSigned = telemetry.deltaSlipAngleSigned - lastTelemetry.deltaSlipAngleSigned;
    telemetry.delta3SlipAngle = telemetry.delta2SlipAngle - lastTelemetry.delta2SlipAngle;
    telemetry.delta3SlipAngleSigned = telemetry.delta2SlipAngleSigned - lastTelemetry.delta2SlipAngleSigned;
    self._calculateSlipForecast(telemetry);

    telemetry.absDeltaSlipAngle = Math.abs(telemetry.deltaSlipAngle);

    self._calculateAndSaveDeceleration(telemetry, lastTelemetry);

    telemetry.avgDecelerationOnStraightPerTick = 0.1; // TODO refactor
    telemetry.calculatedDecelerationCount = self._telemetries.length;

    telemetry.turboAvailable = (turbo !== null && !_.isUndefined(turbo));
    telemetry.turboFactor = (telemetry.turboAvailable) ? turbo.turboFactor : 0.0;

    telemetry.nearestSameLaneOpponentAheadDistance = self._opponentSpy.getSameLaneOpponentAheadDistance();
    telemetry.nearestSameLaneOpponentAheadVelocityDiff = self._opponentSpy.getSameLaneOpponentAheadVelocityDiff();


    self._telemetries.push(telemetry);

    return telemetry;
};

TelemetryCalculator.prototype.appendTelemetryValues = function(source) {
    var self = this;
    var telemetry = self.getLatest();
    _.assign(telemetry, source);
};


TelemetryCalculator.prototype.getAll = function() {
    return this._telemetries;
};


TelemetryCalculator.prototype.getLatest = function() {
    var self = this;
    var lastTelemetry = self._telemetries[self._telemetries.length-1];

    if(_.isUndefined(lastTelemetry)) {
        // Default values to be used for lastTelemetry on start
        lastTelemetry = new Telemetry();
        lastTelemetry.decelerationEquation = self._decelerationEquation;
    }

    return lastTelemetry;
};

TelemetryCalculator.prototype._calculateCurvesTelemetry = function(telemetry, carPosition) {
    var curvesToCalculate = 6;
    telemetry.nextCurves = new Array(curvesToCalculate);

    for (var i = 0; i < curvesToCalculate; i++) {
        var curveInfo = this._trackKnowledge.getNextCurve(carPosition, i);
        var curvePiece = curveInfo.piece;
        var info = {};
        info.curveAngle = curvePiece.angle;
        info.curveSlopeness = Math.abs(curvePiece.slopeness);
        info.curveLaneRadius = curvePiece.laneRadius[telemetry.endLaneIndex];
        info.curveMaxVelocity = curvePiece.laneMaxVelocities[telemetry.endLaneIndex];
        info.curveDistance = curveInfo.distance;
        info.curveTotalAngle = curvePiece.totalAngle;
        telemetry.nextCurves[i] = info;
    }
};

TelemetryCalculator.prototype._calculateSlipForecast = function(telemetry) {
    var ticksToForecastSlip = 20;
    telemetry.slipForecast = new Array(ticksToForecastSlip);
    telemetry.slipForecastSigned = new Array(ticksToForecastSlip);

    var slip = telemetry.slipAngleSigned;
    var deltaSlip = telemetry.deltaSlipAngleSigned;
    var delta2Slip = telemetry.delta2SlipAngleSigned;
    var delta3Slip = telemetry.delta3SlipAngleSigned;

    for(var i=0; i<ticksToForecastSlip; i++)
    {
        slip += deltaSlip;
        deltaSlip += delta2Slip;
        delta2Slip += delta3Slip;
        telemetry.slipForecast[i] = Math.abs(slip);
        telemetry.slipForecastSigned[i] = slip;
    }

};

TelemetryCalculator.prototype._calculateAndSaveDeceleration = function(telemetry, lastTelemetry) {
    var self = this;

    if(self._telemetries.length < 2) { return; }
    var last2Telemetry = self._telemetries[self._telemetries.length-2];

    // Calculate avg deceleration on straight
    if(lastTelemetry.throttle === 0 && last2Telemetry.velocity > 0 && lastTelemetry.velocity > 0 && lastTelemetry.deltaVelocity < 0) {
        self._deceleration.push([last2Telemetry.velocity, -lastTelemetry.deltaVelocity]);
        var numDecelerations = self._deceleration.length;

        if (numDecelerations >= 15) {
            var result = regression('logarithmic', self._deceleration); // Braking is logarithmic. On faster speed, it brakes more.
            // y = a + b ln x
            // a = result.equation[0], b = result.equation[1]
            self._decelerationEquation = result.equation;
            telemetry.decelerationEquation = result.equation;
        }
    }

    telemetry.decelerationEquation = self._decelerationEquation;
    // log.log("a: " + telemetry.decelerationEquation[0] + ", b: " + telemetry.decelerationEquation[1]);
};


//********************************************************************************************************************
// Exports - Class Constructor
//********************************************************************************************************************
module.exports = TelemetryCalculator;
