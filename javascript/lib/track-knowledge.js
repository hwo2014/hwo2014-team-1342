"use strict";

var _ = require('lodash');
var log = require("./race-log");
var regression = require('./regression');

/**
 * Knowledge about track, pieces and lanes.
 * Contains also all the possible paths for a single lap.
 * @constructor
 */
function TrackKnowledge() {
    /**
     * Indicates that learning of initial track data is ready.
     * @member {boolean}
     */
    this._learned = false;
    this._track = null;
    this.numberOfLanes = 0;
    this.pieceLanePaths = null; // path for each the lanes


    // curve radius and max velocity (initial data)
    this._maxCurveVelocities = [
        [220, 10.4], // USA Left track
        [210, 10.3], // Finland outern, France
        [200, 9.6],
        [190, 9.5], // France
        [180, 9.2],
        [110, 6.9], // Finland, France
        [90, 6.1], // Finland, Germany
        [60, 4.9],
        [40, 3.9] // France
    ];

    var result = regression('linear', this._maxCurveVelocities); // Braking is logarithmic. On faster speed, it brakes more.
    this._maxCurveVelocityEquation = result.equation;
    // y = mx + c
    var m = result.equation[0];
    var c = result.equation[1];
    log.log(result.equation[0].toFixed(4) + "x + " + result.equation[1].toFixed(4));

}

TrackKnowledge.prototype.getPiece = function(carPosition, offset) {
    if(_.isUndefined(offset) || offset === null) {
        offset = 0;
    }
    var pieceIndex = carPosition.piecePosition.pieceIndex;
    pieceIndex += offset;
    if(pieceIndex >= this._track.pieces.length) { pieceIndex = 0;}
    var piece = this._track.pieces[pieceIndex];
    return piece;
};

TrackKnowledge.prototype.getDistanceBetween = function(behindCarData, aheadCarData) {
    var self = this;
    var distance = 0.0;
    var pieces = self._track.pieces;
    var i;

    var pieceIndex = behindCarData.pieceIndex;
    if(behindCarData.pieceIndex === aheadCarData.pieceIndex && behindCarData.inPieceDistance > aheadCarData.inPieceDistance) {
        pieceIndex++; // ahead car is actually behind us so the distance is whole the track
        if(pieceIndex >= self.pieceCount()) { pieceIndex = 0;}
    }

    while(pieceIndex !== aheadCarData.pieceIndex) {
        distance += pieces[pieceIndex].laneLengths[behindCarData.startLaneIndex];
        pieceIndex++;
        if(pieceIndex >= self.pieceCount()) { pieceIndex = 0;}
    }

    distance -= behindCarData.inPieceDistance;
    distance += aheadCarData.inPieceDistance;
    return distance;
};

TrackKnowledge.prototype.getInPieceDistanceLeft = function(car) {
    var self = this;

    var piecePosition = car.piecePosition;
    var inPieceDistance = piecePosition.inPieceDistance;
    var lane = piecePosition.lane.startLaneIndex;
    var piece = self._track.pieces[piecePosition.pieceIndex];
    var laneDistance = piece.laneLengths[lane];
    return laneDistance - inPieceDistance;
};

TrackKnowledge.prototype.getNextCurve = function(car, offset) {
    if(_.isUndefined(offset) || offset === null) {
        offset = 0;
    }

    var self = this;
    var piecePosition = car.piecePosition;
    var lane = piecePosition.lane.startLaneIndex;
    var currentPieceIndex = piecePosition.pieceIndex;
    var pieceIndex = currentPieceIndex;
    var distance = 0.0;
    var piece = self._track.pieces[pieceIndex];

    while(!piece.isCurve || pieceIndex === currentPieceIndex || offset > 0) {
        var laneDistance = piece.laneLengths[lane];
        if(pieceIndex === currentPieceIndex) {
            laneDistance -= piecePosition.inPieceDistance;
        }
        distance = distance + laneDistance;

        if(piece.isCurve && pieceIndex !== currentPieceIndex) {
            offset--;
        }
        pieceIndex++;
        if(pieceIndex >= self._track.pieces.length) {pieceIndex = 0;}
        piece = self._track.pieces[pieceIndex];
    }

    return {piece: piece, distance: distance};
};

/**
 *
 * @param {Object} data     Track data got before race starts.
 * @param {Function} callback   Callback function(err, learned)
 */
TrackKnowledge.prototype.learn = function(data, callback) {
    var self = this;
    if(self._learned) {
        return callback(null, false);
    }

    var race = data.race;
    self._track = race.track;
    var track = self._track;
    self.numberOfLanes = track.lanes.length;

    // Make sure lanes are sorted by index.
    track.lanes = _.sortBy(track.lanes, 'index');

    for(var i=0; i<track.pieces.length; i++) {
        var piece = track.pieces[i];
        piece.pieceIndex = i;
        if(_.isUndefined(piece.length)) {
            piece.isCurve = true;
        }
        self._calculateLaneDataForPiece(piece, track.lanes);
        self._calculateSlopeness(piece);
    }

    // Need to loop again because curve angles need to be calculated
    for(i=0; i<track.pieces.length; i++) {
        self._calculateCurveTotalAngle(i);
    }

    self._findPaths(track.pieces);
    self._learned = true;
    return callback(null, true);
};

TrackKnowledge.prototype.learnOnRace = function(ownCarPosition, telemetries) {
    var self = this;
    var tick = telemetries.length-1;
    var telemetry = telemetries[tick];

    // Learn only when piece has been changed
    if(!telemetry.pieceChanged) { return; }


    var previousPiecesToCalculate = 8;
    var maxSlipOnPreviousPieces = 0;
    var maxVelocityOnPreviousPieces = 0;
    var minVelocityOnPreviousPieces = 999;
    var pieceIndex = telemetry.pieceIndex;
    var learnPieceIndex = pieceIndex - previousPiecesToCalculate;
    if(learnPieceIndex < 0) { learnPieceIndex+= self._track.pieces.length; }
    var learnPieceEndLane = null;
    var finalPieceIndex = pieceIndex - (previousPiecesToCalculate + 1);
    if(finalPieceIndex < 0) { finalPieceIndex += self._track.pieces.length; }
    var slipGuardActivated = false;

    if(ownCarPosition.piecePosition.lap === 1 && pieceIndex <= 5 + previousPiecesToCalculate) {
        return; // Don't update data for fist pieces on first lap because we just started to accelerate
    }


    while(tick >= 0 && telemetry.pieceIndex !== finalPieceIndex) {
        if(learnPieceEndLane === null && telemetry.pieceIndex === learnPieceIndex) {
            learnPieceEndLane = telemetry.endLaneIndex;
        }
        var slip = telemetry.slipAngle;
        maxSlipOnPreviousPieces = Math.max(maxSlipOnPreviousPieces, slip);
        minVelocityOnPreviousPieces = Math.min(minVelocityOnPreviousPieces, telemetry.velocity);
        maxVelocityOnPreviousPieces = Math.max(maxVelocityOnPreviousPieces, telemetry.velocity);

        if(!_.isUndefined(telemetry.slipGuardActivated) && telemetry.slipGuardActivated === true) {
            slipGuardActivated = true;
        }

        tick--;
        telemetry = telemetries[tick];
    }

    if(learnPieceEndLane === null) {
        // Start of the race. We have not visited that piece yet.
        return;
    }

    var previousPiece = self._track.pieces[learnPieceIndex];
    var velocityFix = 0;
    if(minVelocityOnPreviousPieces < 0.1 && maxSlipOnPreviousPieces > 55) {
        // Must be crash
        velocityFix = -0.5;
    } else if(slipGuardActivated && maxSlipOnPreviousPieces > 57) {
        log.log("Scaling lane speed down");
        velocityFix = -0.05;
    } else if(maxSlipOnPreviousPieces > 58.5) {
        log.log("Scaling lane speed down (2)");
        velocityFix = -0.05;
    } else if(maxSlipOnPreviousPieces < 56 && maxVelocityOnPreviousPieces >= previousPiece.laneMaxVelocities[learnPieceEndLane]) {
        // Fix only if we have drove faster than limit
        velocityFix = Math.max(0, -0.0077 * maxSlipOnPreviousPieces + 0.4103);
    }

    var oldVelocity = previousPiece.laneMaxVelocities[learnPieceEndLane];
    previousPiece.laneMaxVelocities[learnPieceEndLane] += velocityFix;
    var newVelocity = previousPiece.laneMaxVelocities[learnPieceEndLane];

    log.log("Max Slip on previous pieces: " + maxSlipOnPreviousPieces + " old v: " + oldVelocity + " new v:" + newVelocity + " piece " + learnPieceIndex);
};

TrackKnowledge.prototype.pieceCount = function() {
    return this._track.pieces.length;
};

TrackKnowledge.prototype._findPaths = function(pieces) {
    var self = this;

    var shortestPathDistances = Array(self.numberOfLanes);
    self.pieceLanePaths = Array(self.numberOfLanes);
    for(var i=0; i<self.numberOfLanes; i++) {
        self.pieceLanePaths[i] = {
            pieceIndex: 0,
            previous: null,
            lane: i,
            distanceFromStart: 0
        };

        shortestPathDistances[i] = self._findPieceLaneNodePaths(pieces, self.pieceLanePaths[i], 0, null);
        log.log("Shortest path " + i + ": " + shortestPathDistances[i]);
    }
};

TrackKnowledge.prototype._findPieceLaneNodePaths = function(pieces, pieceLaneNode) {
    var self = this;
    var pieceIndex = pieceLaneNode.pieceIndex;
    var piece = pieces[pieceIndex];
    var nextPieceIndex = pieceIndex + 1;
    if(nextPieceIndex >= pieces.length) { nextPieceIndex = 0; }

    if(pieceIndex === 0 && pieceLaneNode.previous !== null) {
        return pieceLaneNode.distanceFromStart;
    }


    pieceLaneNode.straight = {
        pieceIndex: nextPieceIndex,
        previous: pieceLaneNode,
        lane: pieceLaneNode.lane,
        distanceFromStart: pieceLaneNode.distanceFromStart + piece.laneLengths[pieceLaneNode.lane]
    };

    if(piece.switch === true) {
        if(pieceLaneNode.lane > 0) {
            // Switching to left is possible
            pieceLaneNode.left = {
                pieceIndex: nextPieceIndex,
                previous: pieceLaneNode,
                lane: pieceLaneNode.lane - 1,
                distanceFromStart: pieceLaneNode.distanceFromStart + (piece.laneLengths[pieceLaneNode.lane] + piece.laneLengths[pieceLaneNode.lane-1]) / 2
            };
        }
        if(pieceLaneNode.lane < self.numberOfLanes-1) {
            // Switching to right is possible
            pieceLaneNode.right = {
                pieceIndex: nextPieceIndex,
                previous: pieceLaneNode,
                lane: pieceLaneNode.lane + 1,
                distanceFromStart: pieceLaneNode.distanceFromStart + (piece.laneLengths[pieceLaneNode.lane] + piece.laneLengths[pieceLaneNode.lane+1]) / 2
            };
        }
    }

    pieceLaneNode.straightDistance = self._findPieceLaneNodePaths(pieces, pieceLaneNode.straight);
    pieceLaneNode.leftDistance = Number.MAX_VALUE;
    pieceLaneNode.rightDistance = Number.MAX_VALUE;
    pieceLaneNode.shortestSwitch = 0;
    pieceLaneNode.shortest = pieceLaneNode.straight;

    if(pieceLaneNode.left) {
        pieceLaneNode.leftDistance = self._findPieceLaneNodePaths(pieces, pieceLaneNode.left);
        if(pieceLaneNode.leftDistance < pieceLaneNode.straightDistance) {
            pieceLaneNode.shortestSwitch = -1;
            pieceLaneNode.shortest = pieceLaneNode.left;
        }
    }
    if(pieceLaneNode.right) {
        pieceLaneNode.rightDistance = self._findPieceLaneNodePaths(pieces, pieceLaneNode.right);
        if(pieceLaneNode.rightDistance < pieceLaneNode.straightDistance && pieceLaneNode.rightDistance < pieceLaneNode.leftDistance) {
            pieceLaneNode.shortestSwitch = 1;
            pieceLaneNode.shortest = pieceLaneNode.right;
        }
    }

    return Math.min(pieceLaneNode.leftDistance, pieceLaneNode.straightDistance, pieceLaneNode.rightDistance);
};

/**
 * Calculates length of sector arc.
 * @param {number} angle    Angle in degrees
 * @param {number} radius   Radius
 * @returns {number}        Length of sector arc
 * @private
 */
TrackKnowledge.prototype._arcLength = function(angle, radius) {
    return Math.abs((angle / 360.0) * 2 * Math.PI * radius);
};

TrackKnowledge.prototype._calculateLaneDataForPiece = function(piece, laneInfos) {
    var self = this;

    //TODO: these should be refactored to be properties of laneInfo
    piece.laneLengths = new Array(self.numberOfLanes);
    piece.laneRadius = new Array(self.numberOfLanes);
    piece.laneMaxVelocities = new Array(self.numberOfLanes);


    for(var i=0; i < self.numberOfLanes; i++) {
        if(piece.isCurve) {
            var laneInfo = laneInfos[i];
            var distanceFromCenter = laneInfo.distanceFromCenter;
            var radius = piece.radius;
            var angle = piece.angle;
            if(angle > 0) {
                // Turn to the right. "A positive value tells that the lanes is to the right from the center line" sp
                // distance from the center must be decreased.
                radius -= distanceFromCenter;
            } else {
                // Turn to the left
                radius += distanceFromCenter;
            }
            piece.laneRadius[i] = radius;
            piece.laneLengths[i] = self._arcLength(angle, radius);
            piece.laneMaxVelocities[i] = self._getCurveTargetVelocity(radius);
        } else {
            // This is straight
            piece.laneLengths[i] = piece.length;
            piece.laneMaxVelocities[i] = 999;
        }
    }
};

TrackKnowledge.prototype._getCurveTargetVelocity = function(radius) {
    var equation = this._maxCurveVelocityEquation;
    // y = mx + c
    var m = equation[0];
    var c = equation[1];
    return m * radius + c;
};


TrackKnowledge.prototype._calculateCurveTotalAngle = function(pieceIndex) {
    var piece = this._track.pieces[pieceIndex];
    var originalPiece = piece;
    var lastPiece = null;
    var curveTotalAngle = 0;
    while(piece.isCurve) {
        if(lastPiece !== null &&
            ((piece.angle < 0 && lastPiece.angle > 0) || (piece.angle > 0 && lastPiece.angle < 0))) {
            break;
        }
        curveTotalAngle += Math.abs(piece.angle);
        lastPiece = piece;
        pieceIndex++;
        if(pieceIndex >= this._track.pieces.length) { pieceIndex = 0;}
        piece = this._track.pieces[pieceIndex];
    }
    originalPiece.totalAngle = curveTotalAngle;
};

// slopeness = deltaY & deltaX
TrackKnowledge.prototype._calculateSlopeness = function(piece) {
    var self = this;

    var pieceAngle = piece.angle;
    var pieceAngleAsRad = toRad(pieceAngle);
    var rad270 = toRad(270);

    if(!piece.isCurve) {
        return 0;
    }
    var r = piece.radius;
    var y1 = -r;
    var y2 = r * Math.sin(rad270 + pieceAngleAsRad);
    var x1 = 0;
    var x2 = r * Math.cos(rad270 + pieceAngleAsRad);
    var deltaY = y2-y1;
    var deltaX = x2-x1;

    piece.slopeness = deltaY / deltaX;

};

function toRad(Value) {
    /** Converts numeric degrees to radians */
    return Value * Math.PI / 180;
}


//-----------------------------------------------------------
// Exports - Class Constructor
//-----------------------------------------------------------
module.exports = TrackKnowledge;