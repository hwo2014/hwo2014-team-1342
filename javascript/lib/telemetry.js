"use strict";

var _ = require('lodash');


function Telemetry() {
    this.slipForecast = null;
    this.slipForecastSigned = null;
    this.decelerationEquation = [];
    // y = a + b ln x
    // a = _decelerationEquation[0], b = _decelerationEquation[1]

    this.pieceIndex = -1;
    this.startLaneIndex = null;
    this.pieceChanged = false;
    this.velocity = 0.0;
    this.deltaVelocity = 0.0;
    this.curveDistance = 0;
    this.slipAngle = 0.0;
    this.slipAngleSigned = 0.0;
    this.deltaSlipAngle = 0.0;
    this.deltaSlipAngleSigned = 0.0;
    this.delta2SlipAngle = 0.0;
    this.delta2SlipAngleSigned = 0.0;
    this.delta3SlipAngle = 0.0;
    this.delta3SlipAngleSigned = 0.0;
    this.absDeltaSlipAngle = 0.0;
    this.throttle = 0.0;
    this.avgDecelerationOnStraightPerTick = 0.0;
    this.calculatedDecelerationCount = 0;
    this.turboAvailable = 0;
}

Telemetry.prototype.getDeceleration = function(velocity) {
    // y = a + b ln x    | where y is deceleration and x is velocity
    // a = _decelerationEquation[0], b = _decelerationEquation[1]
    return this.decelerationEquation[0] + this.decelerationEquation[1] * Math.log(velocity);
};

module.exports = Telemetry;