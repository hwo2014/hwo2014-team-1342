"use strict";

var turboHandler = require('./handlers/simple-turbo-handler');
var shortestPathSwitchLaneHandler = require('./handlers/shortest-path-switch-lane-handler');
var slowCarAheadSwitchLaneHandler = require('./handlers/slow-car-ahead-switch-lane-handler');
var curveBumpOvertakeHandler = require('./handlers/curve-bump-overtake-handler');
var curveBumpTurboOvertakeHandler = require('./handlers/curve-bump-turbo-overtake-handler');
var targetCurveSpeedHandler = require('./handlers/target-curve-speed-handler');
var slipGuardHandler = require('./handlers/slip-guard-handler');


module.exports.logic = function(params, resultAction) {
    shortestPathSwitchLaneHandler(params, resultAction);
    slowCarAheadSwitchLaneHandler(params, resultAction);

    targetCurveSpeedHandler(params, resultAction);
    turboHandler(params, resultAction);
    slipGuardHandler(params, resultAction);

    //curveBumpOvertakeHandler(params, resultAction);
    curveBumpTurboOvertakeHandler(params, resultAction);
};

module.exports.name = "Race Logic";
