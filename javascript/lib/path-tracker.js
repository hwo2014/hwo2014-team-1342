"use strict";

var _ = require('lodash');
var util = require('util');

/**
 * Is responsible for tracking the current path.
 * @param {TrackKnowledge} trackKnowledge
 * @constructor
 */
function PathTracker(trackKnowledge) {

    /** @private {TrackKnowledge} */
    this._trackKnowledge = trackKnowledge;

    /** @private {Object} */
    this._lastPieceLanePath = null;

}

/**
 * Returns latest tracked pieceLanePath object.
 * @returns {null|{Object}}
 */
PathTracker.prototype.getCurrent = function() {
    return this._lastPieceLanePath;
};

/**
 * Tracks the path we are going and returns piece lane path
 * @param {Object} carPosition
 */
PathTracker.prototype.trackPath = function(carPosition) {
    var self = this;

    var startLaneIndex = carPosition.piecePosition.lane.startLaneIndex;
    var pieceIndex = carPosition.piecePosition.pieceIndex;
    var pieceLanePath = this._lastPieceLanePath;
    var pieceChanged = pieceLanePath === null || pieceIndex !== pieceLanePath.pieceIndex;

    if(pieceChanged) {
        // We have moved to the next piece. Update pieceLanePath to point to the correct piece with correct lane.

        if(self._lastPieceLanePath === null) {
            pieceLanePath = self._trackKnowledge.pieceLanePaths[startLaneIndex];
        }
        else if(startLaneIndex === pieceLanePath.lane) {
            pieceLanePath = pieceLanePath.straight;
        } else if(startLaneIndex === pieceLanePath.lane-1) {
            pieceLanePath = pieceLanePath.left;
        } else if(startLaneIndex === pieceLanePath.lane+1) {
            pieceLanePath = pieceLanePath.right;
        } else {
            throw new Error("Impossible lane switch!");
        }
        if(_.isUndefined(pieceLanePath)) {
            // End of track. Start from beginning.
            pieceLanePath = self._trackKnowledge.pieceLanePaths[startLaneIndex];
        }
    }

    self._lastPieceLanePath = pieceLanePath;
    return pieceLanePath;
};


//********************************************************************************************************************
// Exports - Class Constructor
//********************************************************************************************************************
module.exports = PathTracker;
