"use strict";

var log = require("./race-log");
var Brain = require("./brain");
var TrackKnowledge = require("./track-knowledge");
var TelemetryCalculator = require("./telemetry-calculator");
var PathTracker = require("./path-tracker");
var OpponentSpy = require("./opponent-spy");
var raceLogic = require('./race-logic');
var testLogic = require('./test-logic');
var slowLogic = require('./slow-logic');
var qualifyingLogic = require('./qualifying-logic');
var _ = require('lodash');
var util = require('util');

/**
 * Main class of the AI.
 * @constructor
 */
function SuperOffRoad() {
    this.logicType = false;
    this._trackKnowledge = new TrackKnowledge();
    this.tournamentState = { isQualifying: false };
    this.brain = null;
    this.myCar = null;
    this._lastPositions = null;
    this._turbo = null;
}

//********************************************************************************************************************
// Event functions related to actual racing
//********************************************************************************************************************

SuperOffRoad.prototype.onCarPositions = function() {
    var self = this;
    return function(data, gameTick, callback) {

        if(!_.isArray(data)) {
            return callback(new Error("Data argument is not an array @ onCarPositions"));
        }

        if(_.isFunction(gameTick)) {
            // There's no game tick on initial positions
            callback = gameTick;
            gameTick = 0;
        }

        self._calculateVelocities(data); //TODO: move to own class

        var positions = self._getPositions(data);
        if(positions === null) {
            return callback(new Error("Unable to find own car position from the data"));
        }

        self._lastOwnCarPosition = positions.ownCar;
        var params = {
            turbo: self._turbo
        };

        self.brain.think(positions.ownCar, positions.opponents, gameTick, params, function(err, result) {
            self._convertSwitchActionToString(result);
            return callback(null, result);
        });
    };
};

SuperOffRoad.prototype.onTurboAvailable  = function() {
    var self = this;
    return function(data, gameTick, callback) {
        if(self.outputType !== "csv") {
            log.log("Turbo available @ " + gameTick + ". Duration: " + data.turboDurationTicks + ", Factor: " + data.turboFactor);
        }
        self._turbo = data;
        return callback(null);
    };
};

SuperOffRoad.prototype.onTurboStart  = function() {
    var self = this;
    return function(data, gameTick, callback) {

        log.log("Turbo start! @" + gameTick + " - " + data.name);

        if(data.name === self.myCar.name && data.color === self.myCar.color) {
            self._turbo = null;
        }

        return callback(null);
    };
};

SuperOffRoad.prototype.onCrash = function() {
    var self = this;
    return function(data, gameTick, callback) {
        if(self.outputType !== "csv") {
            log.log("Crash @ " + gameTick);
        }
        self.brain.throttle = 1.0; //TODO: hack. Should be moved
        return callback(null);
    };
};

SuperOffRoad.prototype.onSpawn  = function() {
    var self = this;
    return function(data, gameTick, callback) {
        if(self.outputType !== "csv") {
            log.log("Spawn @ " + gameTick);
        }
        return callback(null);
    };
};

SuperOffRoad.prototype.onLapFinished  = function() {
    var self = this;
    return function(data, gameTickOrCallback, callback) {
        var gameTick = null;
        if(_.isFunction(gameTickOrCallback)) {
            callback = gameTickOrCallback;
        } else {
            gameTick = gameTickOrCallback;
        }
        if(self.outputType !== "csv") {
            log.log("Lap finished @ " + gameTick);
            log.log(util.inspect((data.lapTime.millis / 1000.0).toFixed(3)));
        }
        return callback(null);
    };
};

SuperOffRoad.prototype.onDnf = function() {
    var self = this;
    return function(data, gameTick, callback) {
        log.log("DNF!");
        return callback(null);
    };
};

SuperOffRoad.prototype.onFinish =  function() {
    var self = this;
    return function(data, gameTick, callback) {
        log.log("Chequered flag!");
        return callback(null);
    };
};

//********************************************************************************************************************
// Event functions not related to actual racing
//********************************************************************************************************************

SuperOffRoad.prototype.onJoin = function() {
    var self = this;
    return function(data, callback) {
        log.log('Joined');
        return callback(null);
    };
};

SuperOffRoad.prototype.onYourCar = function() {
    var self = this;
    return function(data, callback) {
        log.log("Got my car data.");
        self.myCar = data;
        log.log("Name: " + self.myCar.name);
        log.log("Color: " + self.myCar.color);
        return callback(null);
    };
};

/**
 * This is called twice if there's qualifying + race
 * @returns {Function}
 */
SuperOffRoad.prototype.onGameInit  = function() {
    var self = this;
    return function (data, callback) {
        log.log("GameInit");

        var isQualifying = true;
        var raceSession = data.race.raceSession;
        if(_.isUndefined(raceSession)) {
            log.log("raceSession not defined!");
            log.log(util.inspect(data));
        } else {
            if(!_.isUndefined(raceSession.quickRace) && raceSession.quickRace === false) {
                isQualifying = false;
            }
        }

        self.tournamentState.isQualifying = isQualifying;

        log.log("Is qualifying: " + isQualifying);

        self.brain = self._getBrain(self.tournamentState, self._trackKnowledge);

        self._trackKnowledge.learn(data, function (err, learned) {
            if(learned) {
                log.log("Track knowledge learned!");
            } else {
                log.log("Track knowledge was already learned.");
            }

            return callback(null);
        });
    };
};

SuperOffRoad.prototype.onGameEnd = function() {
    var self = this;
    return function(data, callback) {
        log.log('Race ended');
        log.log(util.inspect(data.results));
        log.log(util.inspect(data.bestLaps));

        return callback(null);
    };
};

SuperOffRoad.prototype.onGameStart = function() {
    var self = this;
    return function(data, gameTick, callback) {
        log.log('Race started');

        // There is a conflict between specification and how the actual server seems to work (sends gameTick
        // although not specified). Support both.
        if(_.isFunction(gameTick)) {
            callback = gameTick;
            gameTick = 0;
        }
        return callback(null);
    };
};


SuperOffRoad.prototype.onTournamentEnd = function() {
    var self = this;
    return function(data, callback) {
        return callback(null);
    };
};

/**
 * Calculates velocities for cars to position.velocity for each carPosition.
 * @param {Array} positions     Position data got on carPositions
 * @private
 */
SuperOffRoad.prototype._calculateVelocities = function (positions) {
    var self = this;

    positions.forEach(function(position) {
        var piecePosition = position.piecePosition;
        var velocity = piecePosition.inPieceDistance;
        if(self._lastPositions !== null) {
            var oldPosition = _.find(self._lastPositions, function(lastPosition) {
                return lastPosition.id.name === position.id.name && lastPosition.id.color === position.id.color;
            });
            if(oldPosition === null) {
                log.log("Unable to find old position on _calculateVelocities!");
            } else {
                var oldPiecePosition = oldPosition.piecePosition;
                if(piecePosition.pieceIndex === oldPiecePosition.pieceIndex) {
                    // Same piece than on last tick
                    velocity -= oldPiecePosition.inPieceDistance;
                } else {
                    // We have to be on next piece
                    var oldPieceDistanceLeft = self._trackKnowledge.getInPieceDistanceLeft(oldPosition);
                    velocity += oldPieceDistanceLeft;
                }
            }
        }
        position.velocity = velocity; // distance per tick
    });

    self._lastPositions = positions.slice(); // Copy the array

};

SuperOffRoad.prototype._convertSwitchActionToString = function(action) {
    // Convert switch values from number to string
    var switchNumber = action.switchLane;
    if(switchNumber === -1) {
        action.switchLane = "Left";
    } else if(switchNumber === 1)  {
        action.switchLane = "Right";
    } else if(switchNumber === 0){
        action.switchLane = "";
    } else {
        action.switchLane = null;
    }
};

/**
 * Factory method.
 * Returns brain to be used.
 * @param {Object} tournamentState
 * @param {TrackKnowledge} trackKnowledge
 * @returns {Brain}
 */
SuperOffRoad.prototype._getBrain = function(tournamentState, trackKnowledge) {
    var self = this;
    var existingTelemetryCalculator = null;
    var brainLogic = qualifyingLogic;

    if(this.logicType === "Test" || this.logicType === "True") { // true for backward compatibility
        brainLogic = testLogic;
    } else if(this.logicType === "Slow") {
        brainLogic = slowLogic;
    }
    else {
        if(tournamentState.isQualifying === false) {
            brainLogic = raceLogic;
            if(self.brain !== null) {
                log.log("Using existing telemetry on race.");
                existingTelemetryCalculator = self.brain.getTelemetryCalculator();
            }
        }
    }

    log.log("Using logic: " + brainLogic.name);

    var opponentSpy = new OpponentSpy(trackKnowledge);
    var telemetryCalculator = new TelemetryCalculator(trackKnowledge, opponentSpy, existingTelemetryCalculator);
    var pathTracker = new PathTracker(trackKnowledge);
    var brain = new Brain(trackKnowledge, opponentSpy, telemetryCalculator, pathTracker, brainLogic.logic);
    brain._trackKnowledge = trackKnowledge;
    return brain;
};

/**
 * Gets own car from carPositions data.
 * @param {Array} data
 * @private
 * @return {Number} Index of own car data in the array.
 */
SuperOffRoad.prototype._getOwnCarPositionIndex = function(data) {
    var self = this;
    return _.findIndex(data, function(carPosition) {
        if(_.isUndefined(carPosition.id)) { return null; }
        return carPosition.id.name === self.myCar.name && carPosition.id.color === self.myCar.color;
    });
};

/**
 * Gets own and opponent car positions from the specified position data.
 * @param {Array} data
 * @returns {Object}    { ownCar, [opponents] }
 * @private
 */
SuperOffRoad.prototype._getPositions = function(data) {
    var self = this;
    var ownCarIndex = self._getOwnCarPositionIndex(data);
    if(ownCarIndex < 0) { return null; }
    var ownCarPosition = data[ownCarIndex];
    data.splice(ownCarIndex, 1);
    return {ownCar: ownCarPosition, opponents: data};
};


//********************************************************************************************************************
// Exports - Class Constructor
//********************************************************************************************************************
module.exports = SuperOffRoad;
